# Kubernetes Fundamentals

Setup for the Linux Foundation class as I encounter it..

## Setup
- Install Ansible, Vagrant, VirtualBox
- `make all`
- `ansible-playbook setup.yml`
